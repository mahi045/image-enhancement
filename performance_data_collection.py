import os
import time
import numpy as np
import cv2
import matplotlib.pyplot as plot
from metaheuristic_module import HillClimber_Enhancer, enhanced_pic
from skimage.measure import shannon_entropy as entropy
import sys
from configurations import *

if __name__ == '__main__':
    if len(sys.argv) < 3:
        print('<input image path> <output image path>')
        exit(0)

    img = cv2.imread(sys.argv[1], cv2.IMREAD_COLOR)

    # to store each run's best quality
    best_fitness = []
    # start time calculation
    start = time.time()
    # save all run's generation wise data
    history = []

    enhancer = HillClimber_Enhancer()
    for run in range(NUM_OF_TEST):
        history.append(enhancer.solver(img, NEIGHBORHOOD_SIZE, GEN))
        best_fitness.append(history[-1][-1][1])
 
    # using only the best one to generate picture
    best_solution = history[best_fitness.index(max(best_fitness))][-1][0]
    cv2.imwrite(sys.argv[2], enhanced_pic(img, best_solution), [cv2.IMWRITE_JPEG_QUALITY, 100])
    # end time
    end = time.time()

    # will create report in report dir
    # report dir. must exist
    if not os.path.exists(REPORT_DIR):
        os.mkdir(REPORT_DIR)
    report_path = os.path.join(
        REPORT_DIR, os.path.basename(sys.argv[1]) + '_' +
        str(NEIGHBORHOOD_SIZE) + '_' + str(GEN) + '.txt'
    )    
    with open(report_path, 'w') as f:
        f.write('# num of test, generation, neighborhood size\r\n')
        f.write('{0} {1} {2}\r\n'.format(NUM_OF_TEST, GEN, NEIGHBORHOOD_SIZE))

        f.write('# image width height\r\n')
        f.write('{0} {1}\r\n'.format(img.shape[1], img.shape[0]))

        f.write('# image entropy\r\n')
        f.write('{0}\r\n'.format(entropy(img)))

        f.write('# avg. time\r\n')
        f.write('{0}s\r\n'.format((end - start) / NUM_OF_TEST))
        
        f.write('# best fitness in each run\r\n')
        for fitness in best_fitness:
            f.write('{0} '.format(fitness))

        f.write('\r\n# avg. std. deviation of best fitness\r\n')
        np_ara = np.array(best_fitness)
        f.write('{0} {1}\r\n'.format(np_ara.mean(), np_ara.std()))

        f.write('# best solution\r\n')
        f.write('{0}\r\n'.format(best_solution))

        f.write('# enhanced image entropy\r\n')
        f.write('{0}\r\n'.format(entropy(enhanced_pic(img, best_solution))))

        f.write('\r\n# history\r\n')
        for idx, run_data in enumerate(history):
            f.write('run {0}\r\n'.format(idx + 1))
            for gen_data in run_data:
                f.write('{0}, {1}\r\n'.format(gen_data[0], gen_data[1]))
