import random, math
import numpy as np
import skfuzzy as fuzz
from skimage.measure import shannon_entropy as entropy
import cv2

from deap import base, creator, tools
import global_configurations as GC


color_range = np.arange(0, 256, 1)


class HillClimber_Enhancer:
    def __init__(self, param):
        # contains parameter for the algorithm and to pass to utility function
        self.param = param
        # don't really understand what's the purpose of weight value
        creator.create('MinFit', base.Fitness, weights = (100.0,))
        creator.create('Individual', list, fitness = creator.MinFit)

        self.toolbox = base.Toolbox()
        # not in use currently
        # self.toolbox.register('attribute', random.random)
        # self.toolbox.register('individual', tools.initRepeat, creator.Individual, self.toolbox.attribute, n = SOL_MIN_SIZE)
        # self.toolbox.register('population', tools.initRepeat, list, self.toolbox.individual)
        self.toolbox.register('mutate', self.mutate)
        self.toolbox.register('evaluate', quality)         

    
    # first solution
    def init_solution(self):
        if not self.param.USE_GAUSS_MEMBERSHIP_FUNCTION:
            return [[64, 64, 10], [128, 128, 64], [192, 64, 200]] 
        else:
            return [[64, 64, 10], [128, 128, 64], [192, 64, 200]] 

    # mutation
    def mutate(self, solution):
        large_mutate_chance = random.random()
        if large_mutate_chance > self.param.MEMBERSHIP_SPLIT_PROB:
            change_mean = random.random()
            for i in range(len(solution)):
                if change_mean > 0.5:
                    if random.random() < self.param.CHANGE_PROB and i % 2 == 0:
                        change = random.gauss(self.param.MUTATE_MU, self.param.MUTATE_SIGMA)
                        sign = random.random()
                        change = int(change) if change >= 1.0 else math.ceil(change)
                        if sign < 0.5:
                            solution[i][0] -= change
                            solution[i][1] -= change
                            # solution[i][2] -= change
                        else:
                            solution[i][0] += change
                            solution[i][1] += change
                            # solution[i][2] += change
                else:        
                    if random.random() < self.param.CHANGE_PROB and i % 2 == 1:
                        change = random.gauss(0, self.param.MUTATE_SIGMA)
                        sign = random.random()
                        if sign < 0.5:
                            solution[i][0] -= change
                            solution[i][1] -= change
                            # solution[i][2] -= change
                        else:
                            solution[i][0] += change
                            solution[i][1] += change
                            # solution[i][2] += change
            
            if solution[0][1] > 0:
                solution[0][1] = 0
            if solution[-1][1] < 0:
                solution[-1][1] = 0

            for i in range(0, len(solution)):
                if solution[i][1] <= 0.0:
                        solution[i][1] = math.exp(-5)
                if solution[i][0] <= 0.0:
                        solution[i][0] = math.exp(-5)
                if solution[i][0] >= 255.0:
                        solution[i][0] = 255.0
        else:
            # large mutation split one input membership into two input membership 
            index_to_split = random.randint(0, len(solution)-1)
            if index_to_split == len(solution)-1:
                tmp = solution[index_to_split-1]
                tmp1 = solution[index_to_split]
            else:
                tmp = solution[index_to_split]
                tmp1 = solution[index_to_split+1]

            solution.insert(
                index_to_split+1, [(tmp[0]+tmp1[0])/2, (tmp[1]+tmp1[1])/2, (tmp[2]+tmp1[2])//2] 
            )

        return solution

    # stochastic search
    # returns generation wise (best solution, fitness)
    # 1st entry contains (initial solution, fitness)
    # last entry contains (best solution, fitness)
    def solver(self, img):
        neighborhood_size = self.param.NEIGHBORHOOD_SIZE
        generation = self.param.GEN
        time_limit = self.param.TIME

        history = []

        initial_solution = self.init_solution()
        best_solution = initial_solution.copy()
        best_solution_quality = self.toolbox.evaluate(enhanced_pic(img, initial_solution, self.param))

        history.append((initial_solution.copy(), best_solution_quality))

        if time_limit > 0:
            import time
            begin_time = time.time()
            while time.time() - begin_time < time_limit:
                neighborhood = []

                for _ in range(neighborhood_size):
                    neighborhood.append(self.toolbox.mutate(initial_solution.copy()))
                   
                quality = [self.toolbox.evaluate(enhanced_pic(img, solution, self.param)) for solution in neighborhood]

                initial_solution = neighborhood[quality.index(max(quality))]

                if best_solution_quality < max(quality):
                    best_solution = initial_solution.copy()
                    best_solution_quality = max(quality)

                history.append((initial_solution.copy(), max(quality)))
        else:
            for i in range(generation):
                neighborhood = []

                for _ in range(neighborhood_size):
                    neighborhood.append(self.toolbox.mutate(initial_solution.copy()))
                   
                quality = [self.toolbox.evaluate(enhanced_pic(img, solution, self.param)) for solution in neighborhood]

                initial_solution = neighborhood[quality.index(max(quality))]

                if best_solution_quality < max(quality):
                    best_solution = initial_solution.copy()
                    best_solution_quality = max(quality)

                history.append((initial_solution.copy(), max(quality)))

        history.append((best_solution, best_solution_quality))

        return history


# quality function for image
def quality(img):
    H = entropy(img)
    if len(img.shape) > 2:
        sobel = cv2.Sobel(cv2.cvtColor(img, cv2.COLOR_BGR2HSV)[:, :, 2], cv2.CV_8U, 1, 1, ksize=3)
    else:
        sobel = cv2.Sobel(img, cv2.CV_8U, 1, 1, ksize=3)

    sobel = sobel * (sobel > GC.EDGE_PIX_VAL_THRESHOLD)
    sobel_count = (sobel > GC.EDGE_PIX_VAL_THRESHOLD).sum()

    return (np.log(np.log(sobel.sum()))* sobel_count * H) / (img.shape[0] * img.shape[1])


# apply fuzzy technique on array of pixel
def enhanced_val(pix_val, color_levels, solution):
    det = 0
    crisp = 0
    global color_range
    for i, color_level in enumerate(color_levels):
        level = fuzz.interp_membership(color_range, color_level, pix_val)
        crisp = crisp + level*solution[i][2]
        det = det + level
    
    crisp /= (det + 1e-12)

    assert 0<=crisp.all()<=255

    return crisp


# we will get the enhanced pic from the following function (needed to measure solution's quality)
def enhanced_pic(img, solution, param):
    if len(img.shape) > 2:
        img_hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
        transformed = np.copy(img_hsv)
    else:
        transformed = np.copy(img)
    
    global color_range
    color_levels = []
    for i in range(0, len(solution)):
        if i == 0 or i == len(solution)-1:
            if param.USE_GAUSS_MEMBERSHIP_FUNCTION:
                color_levels.append(fuzz.sigmf(color_range, solution[i][0], solution[i][1]))
            else:
                if i == 0:
                    color_levels.append(fuzz.trapmf(color_range, [0, 0, solution[i][0], solution[i][0] + solution[i][1]]))
                else:
                    color_levels.append(fuzz.trapmf(color_range, [solution[i][0] - solution[i][1], solution[i][0], 255, 255]))
        else:
            if param.USE_GAUSS_MEMBERSHIP_FUNCTION:
                color_levels.append(fuzz.gaussmf(color_range, solution[i][0], solution[i][1]))
            else:
                color_levels.append(fuzz.trimf(color_range, [solution[i][0] - solution[i][1]//2, solution[i][0], solution[i][0] + solution[i][1]//2]))

    if len(img.shape) > 2:
        transformed[:, :, 2] = np.asarray(np.array([enhanced_val(x, color_levels, solution) for x in img_hsv[:, :, 2]]), dtype=np.uint8)
        transformed = cv2.cvtColor(transformed, cv2.COLOR_HSV2BGR)
    else:
        transformed[:, :] = np.asarray(np.array([enhanced_val(x, color_levels, solution) for x in img[:, :]]), dtype=np.uint8)
    
    return transformed

