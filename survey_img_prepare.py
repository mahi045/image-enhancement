import sys
import os
import cv2
import numpy as np

BEST_VARIANTS = []

if __name__ == '__main__':
    if len(sys.argv) < 2:
        print("python3 survey_img_prepare.py <exp_input folder name> <exp_output folder name>")
        exit(0)
    
    if not os.path.exists(sys.argv[1]):
        print("path {0} does not exist".format(sys.argv[1]))
        exit(0)

    root_dir = sys.argv[2]
    if not os.path.exists(root_dir):
        print("path {0} does not exist".format(root_dir))
        exit(0)

    # find the best variants from best_variants.txt
    with open(os.path.join(root_dir, "best_three_variant.txt"), "r") as fin:
        for line in fin.readlines():
            BEST_VARIANTS.append(line.rstrip())
    
    print("best three variants are ", BEST_VARIANTS)
    
    # extract the best run folder name and create a shortcut link of that as run_best 
    for variants in BEST_VARIANTS:
        # do for each image
        # each image name will have a corresponding directory in each variant folder 
        for image_name in os.listdir(sys.argv[1]):
            var_img_folder = os.path.join(os.path.join(root_dir, variants), image_name[:-4])

            # extract the best run norm value from run_best.txt
            best_run_norm_fitness = []
            with open(os.path.join(var_img_folder, "run_best.txt"), "r") as fin:
                for line in fin.readlines():
                    best_run_norm_fitness.append(line.rstrip())

            # now extract norm fitness for each run and check if they are same as best run
            # if they are then it is the best run and similarly create a softlink named run_best
            best_run_dir = ""
            is_symlink_built = False
            for fi_or_fo in os.listdir(var_img_folder):
                # directory of interest is named as run_<number>
                if os.path.isdir(os.path.join(var_img_folder, fi_or_fo)) and fi_or_fo.startswith("run_"):               
                    cur_dir = os.path.join(var_img_folder, fi_or_fo)
                    cur_run_is_best = True
                    with open(os.path.join(cur_dir, fi_or_fo + "_norm_fitness.txt"), "r") as fin:
                        num_of_line = 0
                        for idx, line in enumerate(fin.readlines()):
                            # can compare here line by line
                            # also we have to check if num of line is same 
                            num_of_line += 1
                            # taking advantage if shortcut evaluation
                            if num_of_line > len(best_run_norm_fitness) or best_run_norm_fitness[idx] != line.rstrip():
                                cur_run_is_best = False
                                break
                        if num_of_line != len(best_run_norm_fitness):
                            cur_run_is_best = False

                    if cur_run_is_best:
                        print("best run dir is {0}".format(cur_dir))
                        if os.path.islink(os.path.join(var_img_folder, "run_best")):
                            os.unlink(os.path.join(var_img_folder, "run_best"))
                        os.symlink(fi_or_fo, os.path.join(var_img_folder, "run_best"))
                        is_symlink_built = True
                        break

            # to assert that run dir corresponding to run_best.txt is found
            assert is_symlink_built, "no run directory matches with best run for image {0}".format(var_img_folder)

    # now we will create the survey image by traversing to <variant name>/<image name>/run_best/gen_best/<image_name>_output.jpg
    # survey image is like this left side survey image, 30pixel black frame, right side output image

    # first create a survey image folder if not exist in root directory
    survey_dir = os.path.join(root_dir, "survey_image")
    if not os.path.exists(survey_dir):
        os.mkdir(survey_dir)

    for variants in BEST_VARIANTS:
        survey_variant_dir = os.path.join(survey_dir, variants)
        if not os.path.exists(survey_variant_dir):
            os.mkdir(survey_variant_dir)

        variant_dir = os.path.join(root_dir, variants)
        # do for each image
        # each image name will have a corresponding directory in each variant folder 
        for image_name in os.listdir(sys.argv[1]):
            var_img_folder = os.path.join(variant_dir, image_name[:-4])
            run_best_folder = os.path.join(var_img_folder, "run_best")
            gen_best_folder = os.path.join(run_best_folder, "gen_best")
            
            # create the image
            # read src image
            src_frame = cv2.imread(os.path.join(sys.argv[1], image_name), cv2.IMREAD_COLOR)
            assert src_frame is not None, "{0} not found".format(os.path.join(sys.argv[1], image_name))

            # read output frame
            output_frame = cv2.imread(os.path.join(gen_best_folder, image_name[:-4] + "_output.jpg"), cv2.IMREAD_COLOR)
            assert output_frame is not None, "{0} not found".format(os.path.join(gen_best_folder, image_name[:-4] + "_output.jpg"))

            assert src_frame.shape == output_frame.shape, "input and output image size doesnot match for image {0}".format(image_name)
            # horizontal stacking
            h = src_frame.shape[0]
            # 30 pixel wide
            seperator = np.zeros((h, 30,src_frame.shape[2]))

            survey_frame = np.hstack((src_frame, seperator, output_frame))
            # write the image
            cv2.imwrite(os.path.join(survey_variant_dir, "survey_{0}.jpg".format(image_name[:-4])), survey_frame)

