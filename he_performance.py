import sys
import cv2  
import numpy as np
from metaheuristic_module import quality
from skimage.measure import shannon_entropy as entropy
  
if __name__ == '__main__':
    if len(sys.argv) < 3:
        print('<input image path> <output image path>')
        exit(0)
    # read a image using imread 
    img = cv2.imread(sys.argv[1], 0) 
      
    # creating a Histograms Equalization 
    # of a image using cv2.equalizeHist() 
    equ = cv2.equalizeHist(img)

    quality_original = quality(img)
    entropy_orignal = entropy(img)

    quality_he_enhance = quality(equ)
    entropy_he_enhance = entropy(equ)
    
    print('original entropy, quality : {0}, {1}'.format(entropy_orignal, quality_original))
    print('he enhanced entropy, quality : {0}, {1}'.format(entropy_he_enhance, quality_he_enhance))

    cv2.imwrite(sys.argv[1] + '_he_enhanced.jpg', equ, [cv2.IMWRITE_JPEG_QUALITY, 100])
