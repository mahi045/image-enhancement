import os
import time
import numpy as np
import cv2
from tqdm import tqdm
import matplotlib.pyplot as plt
import hill_climbing_algorithm as hc_module
import genetic_algorithm as ga_module
from skimage.measure import shannon_entropy as entropy
import sys
import skfuzzy as fuzz
import exp_configurations as ECONF


def genwise_img_generation_hc_exp(history, genwise_output_dir, param):
    for generation, solution in enumerate(history[0]):
        if generation == len(history[0]) - 1:
            # create generation wise directory
            if not os.path.exists(os.path.join(genwise_output_dir, "gen_best")):
                os.mkdir(os.path.join(genwise_output_dir, "gen_best"))
            cur_gen_output_dir = os.path.join(genwise_output_dir, "gen_best")
        else:
            # create generation wise directory
            if not os.path.exists(os.path.join(genwise_output_dir, "gen_"+str(generation))):
                os.mkdir(os.path.join(genwise_output_dir, "gen_"+str(generation)))
            cur_gen_output_dir = os.path.join(genwise_output_dir, "gen_"+str(generation))

        cv2.imwrite(os.path.join(cur_gen_output_dir, basename + '_output.jpg'), hc_module.enhanced_pic(img, solution[0], param), [cv2.IMWRITE_JPEG_QUALITY, 100])

        color_range = np.arange(0, 256, 1)
        for i, color_level in enumerate(solution[0]):
            if i == 0 or i == len(solution[0]) - 1:
                if param.USE_GAUSS_MEMBERSHIP_FUNCTION:
                    color = fuzz.sigmf(color_range, color_level[0], color_level[1])
                else:
                    if i == 0:
                        color = fuzz.trapmf(color_range, [0, 0, color_level[0], color_level[0] + color_level[1]])
                    else:
                        color = fuzz.trapmf(color_range, [color_level[0] - color_level[1], color_level[0], 255, 255])
            else:
                if param.USE_GAUSS_MEMBERSHIP_FUNCTION:
                    color = fuzz.gaussmf(color_range, color_level[0], color_level[1])
                else:    
                    color = fuzz.trimf(color_range, [color_level[0] - color_level[1]//2, color_level[0], color_level[0] + color_level[1]//2])
            plt.plot(color_range, color)

        plt.legend([str(color_level[2]) for color_level in solution[0]])
        plt.savefig(os.path.join(cur_gen_output_dir, basename + '_plot.png'), dpi=300)
        plt.clf()
    

def genwise_img_generation_ga_exp(history, genwise_output_dir, param):
    for generation, solution in enumerate(history[0]):
        if generation == len(history[0]) - 1:
            # create generation wise directory
            if not os.path.exists(os.path.join(genwise_output_dir, "gen_best")):
                os.mkdir(os.path.join(genwise_output_dir, "gen_best"))
            cur_gen_output_dir = os.path.join(genwise_output_dir, "gen_best")
        else:
            # create generation wise directory
            if not os.path.exists(os.path.join(genwise_output_dir, "gen_"+str(generation))):
                os.mkdir(os.path.join(genwise_output_dir, "gen_"+str(generation)))
            cur_gen_output_dir = os.path.join(genwise_output_dir, "gen_"+str(generation))

        cv2.imwrite(os.path.join(cur_gen_output_dir, basename + '_output.jpg'), ga_module.enhanced_pic(img, solution[0], param), [cv2.IMWRITE_JPEG_QUALITY, 100])

        color_range = np.arange(0, 256, 1)
        if param.USE_GAUSS_MEMBERSHIP_FUNCTION:
            color_dark = fuzz.sigmf(color_range, solution[0][0], -1/solution[0][1])
            plt.plot(color_range, color_dark, color='b')
            color_grey = fuzz.gaussmf(color_range, solution[0][2], solution[0][3])
            plt.plot(color_range, color_grey, color='g')
            color_bright = fuzz.sigmf(color_range, solution[0][4], 1/solution[0][5])
            plt.plot(color_range, color_bright, color='r')

        else:
            color_dark = fuzz.trapmf(color_range, [0, solution[0][0], solution[0][1], solution[0][2]])
            plt.plot(color_range, color_dark, color='b')
            color_grey = fuzz.trimf(color_range, [solution[0][3], solution[0][4], solution[0][5]])
            plt.plot(color_range, color_grey, color='g')
            color_bright = fuzz.trapmf(color_range, [solution[0][6], solution[0][7], solution[0][8], 255])
            plt.plot(color_range, color_bright, color='r')

        plt.legend(["10", "64", "200"])

        plt.savefig(os.path.join(cur_gen_output_dir, basename + '_plot.png'), dpi=300)
        plt.clf()


def runwise_report_generation(genwise_output_dir, history, img, num_of_test, param):
    if param.TIME > 0:
        report_path = os.path.join(
            os.path.join(genwise_output_dir, basename + '_' +
            str(param.NEIGHBORHOOD_SIZE) + '_time_' + str(param.TIME) + '.txt')
        )
    else:
        report_path = os.path.join(
            os.path.join(genwise_output_dir, basename + '_' +
            str(param.NEIGHBORHOOD_SIZE) + '_gen_' + str(param.GEN) + '.txt')
        )

    with open(report_path, 'w') as f:
        f.write('# num of test, generation, neighborhood size, time_limit\r\n')
        f.write('{0} {1} {2} {3}\r\n'.format(num_of_test, param.GEN, param.NEIGHBORHOOD_SIZE, param.TIME))

        f.write('# image width height\r\n')
        f.write('{0} {1}\r\n'.format(img.shape[1], img.shape[0]))

        f.write('# image entropy\r\n')
        f.write('{0}\r\n'.format(entropy(img)))

        f.write('# avg. time\r\n')
        f.write('{0}s\r\n'.format((end - start) / num_of_test))
        
        f.write('\r\n# history\r\n')
        for idx, run_data in enumerate(history):
            f.write('run {0}\r\n'.format(idx + 1))
            for gen_data in run_data:
                f.write('{0}, {1}\r\n'.format(gen_data[0], gen_data[1]))


def runwise_norm_fitness_report_generation(runwise_output_dir, run, history):
    report_path = os.path.join(
        os.path.join(genwise_output_dir, 
        'run_' + str(run) + '_norm_fitness.txt')
    )
    fitness_history = [data[1] for data in history[0]]

    max_fitness = max(fitness_history)
    min_fitness = min(fitness_history)
    
    # last entry contains the (best solution, best fitness)
    generation_count = len(fitness_history) - 1
    
    with open(report_path, 'w') as f:
        for idx, run_data in enumerate(history):
            for gen_data in run_data:
                if max_fitness - min_fitness > 0:
                    norm_fitness = (gen_data[1] - min_fitness)/(max_fitness - min_fitness)
                else:                    
                    norm_fitness = 0
                f.write('{0}\r\n'.format(norm_fitness))
    # return average increament per generation
    return (max_fitness - min_fitness) / generation_count 


if __name__ == '__main__':
    if len(sys.argv) < 3:
        print('<input image dir> <algorithm name>')
        exit(0)

    assert sys.argv[2] in ECONF.experiment_conf["algo_conf_list"].keys(), \
    "{0} is not a suitable algorithm name, valid names are {1}".format(sys.argv[2], list(ECONF.experiment_conf["algo_conf_list"].keys()))

    # create output directory for the experiment
    if not os.path.exists(sys.argv[2]):
        os.mkdir(sys.argv[2])

    algo_conf = ECONF.experiment_conf["algo_conf_list"][sys.argv[2]]
    # declare the optimizer
    if algo_conf["use_hc"]:
        enhancer = hc_module.HillClimber_Enhancer(param=algo_conf["param_class"])
    else:
        enhancer = ga_module.GeneticAlgorithm_Enhancer(param=algo_conf["param_class"])
    
    # to store best quality from runs on an image
    imgwise_best_genwise_avg_norm_fitness_increment = []

    for filename in tqdm(os.listdir(sys.argv[1])):
        basename = os.path.basename(filename)[:-4]
        img = cv2.imread(os.path.join(sys.argv[1], filename))

        # save all run's generation wise data
        history = []

        # all experiment according to configuration.NUM_OF_TEST
        # start time calculation
        start = time.time()
        for run in range(ECONF.experiment_conf["num_of_test"]):
            history.append([])
            history[run].append(enhancer.solver(img))
        end = time.time()

        
        # folder structure contains following files
        # <output dir>/<image name>/<run#>/<gen#>/<image file(jpg)>
        # <output dir>/<image name>/<run#>/<gen#>/<fuzzy set graph(png)>
        # <output dir>/<image name>/<run#>/<genwise_report.txt>
        # <output dir>/<image name>/<run#_fitness.txt>

        # generate runwise and then generation wise image report of each input image
        # first create a seperate directory for the image inside output_dir, this will contain individual run dir
        if not os.path.exists(os.path.join(sys.argv[2], basename)):
            os.mkdir(os.path.join(sys.argv[2], basename))
            
        # generate runwise and then genwise report
        runwise_output_dir = os.path.join(sys.argv[2], basename)
        best_genwise_avg_fitness_increment = None
        best_run_idx = None

        for run in range(ECONF.experiment_conf["num_of_test"]):      
            genwise_output_dir = os.path.join(runwise_output_dir, "run_"+str(run))
            if not os.path.exists(genwise_output_dir):
                os.mkdir(genwise_output_dir)
            # create generation wise output genotype and phenotype
            if algo_conf["use_hc"]:
                genwise_img_generation_hc_exp(history[run], genwise_output_dir, algo_conf["param_class"])
            else:
                genwise_img_generation_ga_exp(history[run], genwise_output_dir, algo_conf["param_class"])
                        
            runwise_report_generation(genwise_output_dir, history[run], img, ECONF.experiment_conf["num_of_test"], algo_conf["param_class"])
            tmp_best_genwise_avg_fitness_increment = \
                runwise_norm_fitness_report_generation(runwise_output_dir, run, history[run])

            if best_genwise_avg_fitness_increment is None or \
                best_genwise_avg_fitness_increment < tmp_best_genwise_avg_fitness_increment:
                best_genwise_avg_fitness_increment = tmp_best_genwise_avg_fitness_increment
                best_run_idx = run

        imgwise_best_genwise_avg_norm_fitness_increment.append(best_genwise_avg_fitness_increment)
        
        # to select the best algorithm we will compare this metric of different algorithm
        with open(os.path.join(runwise_output_dir, 'run_best.txt'), 'w') as fout:
            fitness_history = [data[1] for data in history[run][0]]

            max_fitness = max(fitness_history)
            min_fitness = min(fitness_history)
            # last entry contains the (best solution, best fitness)
            generation_count = len(fitness_history) - 1
        
            for idx, run_data in enumerate(history[run]):
                for gen_data in run_data:
                    if max_fitness - min_fitness > 0:
                        norm_fitness = (gen_data[1] - min_fitness)/(max_fitness - min_fitness)
                    else:                    
                        norm_fitness = 0
                    fout.write('{0}\r\n'.format(norm_fitness))

    # to select the best algorithm we will compare this metric of different algorithm
    with open(os.path.join(sys.argv[2], 'comp_metric.txt'), 'w') as fout:
        comp_metric = sum(imgwise_best_genwise_avg_norm_fitness_increment) / len(imgwise_best_genwise_avg_norm_fitness_increment)
        fout.write(str(comp_metric))

