import random, math
import numpy as np
import skfuzzy as fuzz
from skimage.measure import shannon_entropy as entropy
import cv2

from deap import base, creator, tools
import global_configurations as GC


color_range = np.arange(0, 256, 1)


class GeneticAlgorithm_Enhancer:
    def __init__(self, param):
        # contains parameter for the algorithm and to pass to utility function
        self.param = param
        # don't really understand what's the purpose of weight value
        creator.create('MinFit', base.Fitness, weights = (100.0,))
        creator.create('Individual', list, fitness = creator.MinFit)

        self.toolbox = base.Toolbox()
        # not in use currently
        # self.toolbox.register('attribute', random.random)
        # self.toolbox.register('individual', tools.initRepeat, creator.Individual, self.toolbox.attribute, n = SOL_MIN_SIZE)
        # self.toolbox.register('population', tools.initRepeat, list, self.toolbox.individual)
        self.toolbox.register('mutate', self.mutate)
        self.toolbox.register('evaluate', quality)  
        self.toolbox.register('crossover', self.uniform_crossover)       

    # first solution
    def init_solution(self):
        if not self.param.USE_GAUSS_MEMBERSHIP_FUNCTION:
            initial_solution = [0, 64+random_width(), 128+random_width(), \
                64+random_width(), 128+random_width(), 192+random_width(), 128+random_width(), \
                192+random_width(), 255] 
            condition_check(initial_solution)
            return initial_solution
        else:
            return [96, 8, 128, 32, 160, 8] 

    
    # check boundary and inequality condition
    def condition_check(self, solution):
        for i in range(0, len(solution), 3):
            if self.param.PREVIOUS_VERSION: 
                if i == 0:
                    solution[i] = 0
                elif i + 2 == 8:
                    solution[i + 2] = 255
                # continue
            if solution[i] < 0:
                solution[i] = 0
            if solution[i+1] < solution[i]:
                solution[i+1] = solution[i] + 1
            if solution[i+2] < solution[i+1]:
                solution[i+2] = solution[i+1] + 1
            solution[i] = 255 if solution[i] > 255 else solution[i] 
            solution[i+1] = 255 if solution[i+1] > 255 else solution[i+1] 
            solution[i+2] = 255 if solution[i+2] > 255 else solution[i+2] 

    # mutation
    def mutate(self, solution):
        if not self.param.USE_GAUSS_MEMBERSHIP_FUNCTION:
            for i in range(len(solution)):
                if (i == 0 or i == 8) and self.param.PREVIOUS_VERSION:
                    continue
                if random.random() < self.param.CHANGE_PROB:
                    change = random.gauss(self.param.MUTATE_MU, self.param.MUTATE_SIGMA)
                    sign = random.random()
                    if sign < 0.5:
                        solution[i] -= int(change)
                    else:
                        solution[i] += int(change)
            condition_check(solution)
            
        else:
            change_mean = random.random()
            for i in range(len(solution)):
                if change_mean > 0.5:
                    if random.random() < self.param.CHANGE_PROB and i % 2 == 0:
                        change = random.gauss(self.param.MUTATE_MU, self.param.MUTATE_SIGMA)
                        sign = random.random()
                        change = int(change) if change >= 1.0 else math.ceil(change)
                        if sign < 0.5:
                            solution[i] -= change
                        else:
                            solution[i] += change
                else:        
                    if random.random() < self.param.CHANGE_PROB and i % 2 == 1:
                        change = random.gauss(0, self.param.MUTATE_SIGMA)
                        sign = random.random()
                        if sign < 0.5:
                            solution[i] -= change
                        else:
                            solution[i] += change 
            for _ in range(1, len(solution), 2):
                if solution[_] <= 0.0:
                    solution[_] = math.exp(-5)
            for _ in range(0, len(solution), 2):
                if solution[_] <= 0.0:
                    solution[_] = math.exp(-5)
            for _ in range(0, len(solution), 2):
                if solution[_] >= 255.0:
                    solution[_] = 255.0
            
                for i in range(2, len(solution)-2, 2):
                    if i != 2:
                        if solution[i]-solution[i+1]//2 > solution[i-2] + solution[i-1]:
                            solution[i+1] = 2*(solution[i] - (solution[i-2] + solution[i-1]//2 - 1))
                        if solution[i]-solution[i+1]//2 < 0:
                            solution[i+1] = 2*solution[i]
                        if solution[i]+solution[i+1]//2 > 255:
                            solution[i+1] = 2*(255-solution[i])

        return solution


        

    def uniform_crossover(self, ind1, ind2):
        assert(len(ind1) == len(ind2))
        divisor = 2 if self.param.USE_GAUSS_MEMBERSHIP_FUNCTION else 3
        assert(len(ind1) % divisor == 0)
        assert(len(ind2) % divisor == 0)

        for _ in range(0, len(ind1), divisor):
            if random.random() > self.param.CHANGE_PROB:
                ind1[_], ind2[_], ind1[_+1], ind2[_+1] = ind2[_], ind1[_], ind2[_+1], ind1[_+1]
                if not self.param.USE_GAUSS_MEMBERSHIP_FUNCTION:
                    ind1[_+2], ind2[_+2] = ind2[_+2], ind1[_+2]
                    

    def sample_wr(self, population):
        "Chooses two random elements (with replacement) from a population"
        result = [0] * 2
        n = len(population)
        _random, _int = random.random, int  # speed hack 
        j = _int(_random() * n)
        result[0] = population[j]
        population[j], population[n-1] = population[n-1], population[j] 
        population.pop()
        
        n = n - 1
        j = _int(_random() * n)
        result[1] = population[j]
        population[j], population[n-1] = population[n-1], population[j] 
        population.pop()
        return result

    # stochastic search
    # returns generation wise (best solution, fitness)
    # 1st entry contains (initial solution, fitness)
    # last entry contains (best solution, fitness)
    def solver(self, img):
        popsize = self.param.POP_SIZE
        generation = self.param.GEN
        time_limit = self.param.TIME

        history = []
        initial_solution = list()
        for _ in range(popsize):
            initial_solution.append(self.init_solution())
        assert(len(initial_solution) == popsize)
        populations = list(initial_solution)
        best_solution = initial_solution[0]

        best_solution_quality = self.toolbox.evaluate(enhanced_pic(img, best_solution, self.param))

        history.append((best_solution.copy(), best_solution_quality))

        if time_limit > 0:
            import time
            begin_time = time.time()
            while time.time() - begin_time < time_limit:
                quality = [self.toolbox.evaluate(enhanced_pic(img, solution, self.param)) for solution in populations]
                
                if best_solution_quality < max(quality):
                    # print(max(quality))
                    best_solution = populations[quality.index(max(quality))]
                    best_solution_quality = max(quality)
                history.append((best_solution.copy(), max(quality)))

                if (len(populations) > popsize):
                    populations = [x for _,x in sorted(zip(quality, populations))]
                    populations = populations[len(populations) // 2:]

                    assert(len(populations) == popsize)

                temp = list()
                current_generation = populations.copy()
                for _ in range(1, popsize, 2):
                    parents = self.sample_wr(populations)
                    
                    child_a = parents[0].copy()
                    child_b = parents[1].copy()
                    self.toolbox.crossover(child_a, child_b)
                    mu_child_a = self.toolbox.mutate(child_a)
                    mu_child_b = self.toolbox.mutate(child_b)
                    # if count < 5:
                    #     print("After change: ", mu_child_a, mu_child_b)
                    temp.append(mu_child_a)
                    temp.append(mu_child_b)
                assert(len(populations) == 0)
                if self.param.EXPLORATIVE:
                    populations = temp.copy()
                    assert(len(populations) == popsize)
                else:
                    populations = temp.copy() + current_generation.copy()
                    assert len(populations) == popsize * 2, print(len(populations))
                # print(populations)
        else:
            for i in range(generation):
                quality = [self.toolbox.evaluate(enhanced_pic(img, solution, self.param)) for solution in populations]
                
                if best_solution_quality < max(quality):
                    # print(max(quality))
                    best_solution = populations[quality.index(max(quality))]
                    best_solution_quality = max(quality)
                history.append((best_solution.copy(), max(quality)))

                if (len(populations) > popsize):
                    populations = [x for _,x in sorted(zip(quality, populations))]
                    populations = populations[len(populations) // 2:]

                    assert(len(populations) == popsize)

                temp = list()
                current_generation = populations.copy()
                for _ in range(1, popsize, 2):
                    parents = sample_wr(populations)
                    
                    child_a = parents[0].copy()
                    child_b = parents[1].copy()
                    self.toolbox.crossover(child_a, child_b)
                    mu_child_a = self.toolbox.mutate(child_a)
                    mu_child_b = self.toolbox.mutate(child_b)
                    # if count < 5:
                    #     print("After change: ", mu_child_a, mu_child_b)
                    temp.append(mu_child_a)
                    temp.append(mu_child_b)
                assert(len(populations) == 0)
                if self.param.EXPLORATIVE:
                    populations = temp.copy()
                    assert(len(populations) == popsize)
                else:
                    populations = temp.copy() + current_generation.copy()
                    assert(len(populations) == popsize * 2)
                # print(populations)

        history.append((best_solution, best_solution_quality))

        return history

def get_gray_level(dark, bright):
    if dark >= DOMINANT_LEV_THRESHOLD or bright >= DOMINANT_LEV_THRESHOLD:
        return min(1-dark, 1-bright)
    elif dark < NON_DOM_LEV_THRESHOLD and bright < NON_DOM_LEV_THRESHOLD:
        return max(1-dark, 1-bright)
    else:
        return (dark + bright) / 2.0

# quality function for image
def quality(img):
    H = entropy(img)
    if len(img.shape) > 2:
        sobel = cv2.Sobel(cv2.cvtColor(img, cv2.COLOR_BGR2HSV)[:, :, 2], cv2.CV_8U, 1, 1, ksize=3)
    else:
        sobel = cv2.Sobel(img, cv2.CV_8U, 1, 1, ksize=3)

    sobel = sobel * (sobel > GC.EDGE_PIX_VAL_THRESHOLD)
    sobel_count = (sobel > GC.EDGE_PIX_VAL_THRESHOLD).sum()

    return (np.log(np.log(sobel.sum()))* sobel_count * H) / (img.shape[0] * img.shape[1])


# apply fuzzy technique on array of pixel
def enhanced_val(pix_val, color_dark, color_grey, color_bright):
    global color_range
    dark_level = fuzz.interp_membership(color_range, color_dark, pix_val)
    grey_level = fuzz.interp_membership(color_range, color_grey, pix_val)
    bright_level = fuzz.interp_membership(color_range, color_bright, pix_val)

    crisp = (dark_level * 10 + grey_level * 64 + bright_level * 200) / (dark_level + grey_level + bright_level)
    
    return crisp


# we will get the enhanced pic from the following function (needed to measure solution's quality)
def enhanced_pic(img, solution, param):
    if len(img.shape) > 2:
        img_hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
        transformed = np.copy(img_hsv)
    else:
        transformed = np.copy(img)

    global color_range
    if not param.USE_GAUSS_MEMBERSHIP_FUNCTION:
        color_dark = fuzz.trapmf(color_range, [0, solution[0], solution[1], solution[2]])
        color_grey = fuzz.trimf(color_range, [solution[3], solution[4], solution[5]])
        color_bright = fuzz.trapmf(color_range, [solution[6], solution[7], solution[8], 255])
    else:
        color_dark = fuzz.sigmf(color_range, solution[0], 1/-solution[1])
        color_grey = fuzz.gaussmf(color_range, solution[2], solution[3])
        color_bright = fuzz.sigmf(color_range, solution[4], 1/solution[5])

    if len(img.shape) > 2:
        transformed[:, :, 2] = np.asarray(np.array([enhanced_val(x, color_dark, color_grey, color_bright) for x in img_hsv[:, :, 2]]), dtype=np.uint8)
        transformed = cv2.cvtColor(transformed, cv2.COLOR_HSV2BGR)
    else:
        transformed[:, :] = np.asarray(np.array([enhanced_val(x, color_dark, color_grey, color_bright) for x in img[:, :]]), dtype=np.uint8)
    
    return transformed

