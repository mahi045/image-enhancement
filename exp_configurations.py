import global_configurations as GC


class HCConf:
    def __init__(self, sol_min_size = 6, change_prob = 0.5, membership_split_prob = 0.1, 
        mutate_mu = 3, mutate_sigma = 2, use_gauss_membership_function = False,  
        trap_tri_range = 50, time = 10, gen = 50, neighborhood_size = 10):
        self.SOL_MIN_SIZE = sol_min_size
        self.CHANGE_PROB = change_prob
        self.MEMBERSHIP_SPLIT_PROB = membership_split_prob
        self.MUTATE_MU = mutate_mu
        self.MUTATE_SIGMA = mutate_sigma
        self.USE_GAUSS_MEMBERSHIP_FUNCTION = use_gauss_membership_function
        self.TRAP_TRI_RANGE = trap_tri_range        
        self.TIME = time
        self.GEN = gen
        self.NEIGHBORHOOD_SIZE = neighborhood_size

class GAConf:
    def __init__(self, sol_min_size = 6, change_prob = 0.5, membership_split_prob = 0.1, 
        mutate_mu = 3, mutate_sigma = 2, use_gauss_membership_function = False, previous_version = True, 
        explorative = False, time = 10, gen = 25, pop_size = 30):
        self.SOL_MIN_SIZE = sol_min_size
        self.CHANGE_PROB = change_prob
        self.MEMBERSHIP_SPLIT_PROB = membership_split_prob
        self.MUTATE_MU = mutate_mu
        self.MUTATE_SIGMA = mutate_sigma
        self.USE_GAUSS_MEMBERSHIP_FUNCTION = use_gauss_membership_function
        self.PREVIOUS_VERSION = previous_version
        self.EXPLORATIVE = explorative
        self.TIME = time
        self.GEN = gen
        self.POP_SIZE = pop_size
        self.NEIGHBORHOOD_SIZE = pop_size


experiment_conf = {
    "num_of_test": GC.NUM_OF_TEST,
    "algo_conf_list": {
        "hc_simple": {
            "use_hc": True, 
            "param_class": HCConf(sol_min_size = 6, change_prob = 0.5, membership_split_prob = 0.0, 
            mutate_mu = 3, mutate_sigma = 2, use_gauss_membership_function = False, 
            trap_tri_range = 50, time = GC.PER_RUN_TIME_SEC, gen = 50, neighborhood_size = 10)},

        "hc_trap_split": {
            "use_hc": True, 
            "param_class": HCConf(sol_min_size = 6, change_prob = 0.5, membership_split_prob = 0.1, 
            mutate_mu = 3, mutate_sigma = 2, use_gauss_membership_function = False,  
            trap_tri_range = 50, time = GC.PER_RUN_TIME_SEC, gen = 50, neighborhood_size = 10)},

        "hc_gauss_split": {
            "use_hc": True, 
            "param_class": HCConf(sol_min_size = 6, change_prob = 0.5, membership_split_prob = 0.1, 
            mutate_mu = 3, mutate_sigma = 2, use_gauss_membership_function = True,  
            trap_tri_range = 50, time = GC.PER_RUN_TIME_SEC, gen = 50, neighborhood_size = 10)},

        "ga_elitist": {
            "use_hc": False, 
            "param_class": GAConf(sol_min_size = 6, change_prob = 0.5, membership_split_prob = 0.1, 
            mutate_mu = 3, mutate_sigma = 2, use_gauss_membership_function = True, previous_version = True, 
            explorative = False, time = GC.PER_RUN_TIME_SEC, gen = 25, pop_size = 30)},

        "ga_explorative": {
            "use_hc": False, 
            "param_class": GAConf(sol_min_size = 6, change_prob = 0.5, membership_split_prob = 0.1, 
            mutate_mu = 3, mutate_sigma = 2, use_gauss_membership_function = True, previous_version = True, 
            explorative = True, time = GC.PER_RUN_TIME_SEC, gen = 25, pop_size = 30)}
    }
}
