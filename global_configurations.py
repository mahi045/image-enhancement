import numpy as np
import random

# # of attribute in a solution
# SOL_MIN_SIZE = 6
# prob. of mutation in an attribute
# CHANGE_PROB = 0.5
# prob. of splitting of membership function
# MEMBERSHIP_SPLIT_PROB = 0.1

# mu and sigma of gaussian distribution of mutation 
# MUTATE_MU = 3
# MUTATE_SIGMA = 2

# used to determine which value(s) will be taken as edge pixel
EDGE_PIX_VAL_THRESHOLD = 0
# USE_GAUSS_MEMBERSHIP_FUNCTION = False
# USE_HC = True
# PREVIOUS_VERSION = True

# controls (1, 1) or (1 + 1) in genetic algorithm
# EXPLORATIVE = False

# numpy array to use in fuzzy method
color_range = np.arange(0, 256, 1)

#
# TRAP_TRI_RANGE = 50

# function used in initiating solution in genetic algo
def random_width():
    return random.randint(1, TRAP_TRI_RANGE) - TRAP_TRI_RANGE // 2

# report file will be created as <input file name>.txt
# REPORT_DIR = 'report'

# TIME = 10
# GEN = 50
# NEIGHBORHOOD_SIZE = 10
# GEN_ALGO_GEN = 25
# POP_SIZE = 30
# multiple test to report avg.time
# we will use best of the run result to create pic
# All five run's best quality will be used to report avg. achieved fitness and it's std. deviation
NUM_OF_TEST = 5

PER_RUN_TIME_SEC = 120
